function newBoard(boardSize) {
  var board = []

  for (var i=0; i<boardSize; i++) {
    var boardRow = []
    for (var j=0; j<boardSize; j++) {
      boardRow.push('')
    }
    board.push(boardRow)
  }
  return board
}
// newBoard(boardSize)


function placeMine(totalMines, boardSize) {
  var mineChance = totalMines / (boardSize * boardSize)
  return Math.random() < mineChance ? 1 : 0
}

function placeMinesOnBoard(boardSize, totalMines) {
  var gameBoard = []
  var minesPlaced = 0

  for (var i=0; i<boardSize; i++) {
    var boardRow = []
    for (var j=0; j<boardSize; j++) {
      boardRow.push(placeMine(totalMines, boardSize))
    }
    minesPlaced += boardRow.reduce(function(acc, val) {
      return acc + val
    })
    gameBoard.push(boardRow)
  }
  // console.log('mines', gameBoard)
  // console.log('mines placed:', minesPlaced)
  return {
    board: gameBoard,
    mines: minesPlaced
  }
}
// var gameBoard = placeMinesOnBoard()


function adjacentMines(gameBoard, boardSize, i, j) {
  var sum = 0
  if (i<boardSize-1) {
    sum += gameBoard[i+1][j]

    if (j<boardSize-1) {
      sum += gameBoard[i+1][j+1]
    }
    if (j>0) {
      sum += gameBoard[i+1][j-1]
    }
  }
  if (j<boardSize-1) {
    sum += gameBoard[i][j+1]
  }

  if (i>0) {
    sum += gameBoard[i-1][j]

    if (j>0) {
      sum += gameBoard[i-1][j-1]
    }
    if (j<boardSize-1) {
      sum += gameBoard[i-1][j+1]
    }
  }
  if (j>0) {
    sum += gameBoard[i][j-1]
  }


  return sum
}

function adjacentMineBoard(gameBoard, boardSize) {
  var playerBoard = []

  for (var i=0; i<boardSize; i++) {
    var playerRow = []
    for (var j=0; j<boardSize; j++) {
      playerRow.push(adjacentMines(gameBoard, boardSize, i, j))
    }
    playerBoard.push(playerRow)
  }
  // console.log('adjacent mines', playerBoard)
  return playerBoard
}
// adjacentMineBoard(gameBoard, boardSize)
