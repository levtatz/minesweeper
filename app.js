class Minesweeper extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      gameOver: false,
      minesLeft: this.props.totalMines,
      board: this.props.board,
      mineBoard: this.props.mineBoard,
      adjacentMineBoard: adjacentMineBoard(this.props.mineBoard, this.props.boardSize)
    }
    this.handleFlag = this.handleFlag.bind(this)
    this.handleReveal = this.handleReveal.bind(this)
  }
  handleReveal(e) {
    const row = parseInt(e.target.parentNode.id)
    const col = parseInt(e.target.id)
    let board = this.state.board

    if (this.state.mineBoard[row][col] != 1) {
      board = this.reveal(board, row, col)
    } else {
      board[row][col] = this.explodeSymbol()
      this.setState({ gameOver: true })
    }

    this.setState({ board })
  }
  reveal(board, row, col) {
    if (this.state.adjacentMineBoard[row][col] == 0) {
      if (row<this.props.boardSize-1 && col<this.props.boardSize-1) {
        this.reveal(board, row+1, col)
        this.reveal(board, row, col+1)
      }
    }
    board[row][col] = this.state.adjacentMineBoard[row][col]
    return board
  }
  handleFlag(e) {
    e.preventDefault()
    const row = e.target.parentNode.id
    const col = e.target.id

    const board = this.state.board
    board[row][col] = this.flagSymbol()
    this.setState({ board })

    if (this.state.mineBoard[row][col] == 1) {
      const minesLeft = this.state.minesLeft - 1
      this.setState({ minesLeft })
    }
  }
  displayBoard() {
    return this.state.board.map(function(row, i) {
      return React.createElement('div', {
        className: 'row',
        id: i,
        key: i
      },
        row.map(function(cell, j) {
          return React.createElement('span', {
            className: 'cell cellType' + cell,
            id: j,
            key: j,
            onClick: (e) => this.state.gameOver ? null : this.handleReveal(e),
            onContextMenu: (e) => this.state.gameOver ? null : this.handleFlag(e),
          }, cell)
        }.bind(this))
      )
    }.bind(this))
  }
  flagSymbol() {
    return String.fromCharCode(9873)
  }
  explodeSymbol() {
    return String.fromCharCode(10005)
  }
  render() {
    return React.createElement('div', {},
      this.displayBoard(),
      this.state.gameOver
      ? React.createElement('p', { className: 'msg nay' }, 'Game Over!')
      : this.state.minesLeft
        ? React.createElement('p', {}, 'Mines left: ', this.state.minesLeft)
        : React.createElement('p', { className: 'msg yay' }, 'You Won!')
    )
  }
}
